module org.egedede.todojrnl {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.egedede.todojrnl to javafx.fxml;
    exports org.egedede.todojrnl;
}
